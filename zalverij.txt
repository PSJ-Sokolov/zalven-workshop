                      ━━━━━━━━━━━━━━━━━━━━━━━━━━━
                       ZALVEN, CRÈMES EN BALSEMS

                              PSJ Sokolov
                      ━━━━━━━━━━━━━━━━━━━━━━━━━━━


                            <2020-01-08 wo>


Inhoudsopgave
─────────────

1. Materialen
.. 1. Kommetjes
.. 2. Mixer
.. 3. Glaswerk
.. 4. zetpil houder
.. 5. Cacoaboter tabletten van “[Super Foodies]”
2. De verschillen tussen zalven, crèmes en balsems
3. Bereiding
.. 1. Hitte
.. 2. Mixen
4. De verschillen tussen cacaoboter en kokosolie
5. Receptuur van ons monster
6. Ingredienten
7. Veganisme
8. Policies


1 Materialen
════════════

  Materialen die we kunnen aanschaffen.


1.1 Kommetjes
─────────────

  :action: Action.


1.2 Mixer
─────────

  :action: Action.


1.3 Glaswerk
────────────

  :rosa-heinz: Glaswerk kan worden gekocht bij Rosa Heinz. Ze hebben ook
  pompjes en viola glas.


1.4 zetpil houder
─────────────────

  `Gaat nog gemaild worden.'


1.5 Cacoaboter tabletten van “[Super Foodies]”
──────────────────────────────────────────────

  Bij de cursus hadden ze cacaoboter in voorgedoseerde tabletten, dit
  lijkt me handig omdat je dat zelf niet meer de cacaoboter fijn hoeft
  te maken, en de hoeveelheden in standaard tabletten kunt aangeven.


[Super Foodies] <https://superfoodies.com/products/cacao-butter>


2 De verschillen tussen zalven, crèmes en balsems
═════════════════════════════════════════════════

  Crèmes zijn zalven met extra water, om het middel stabiel te houden
  heb je een emulgator en een stabilisator nodig (maizena). Balsems zijn
  zalven met extra bijenwas of andere harde stoffen, zalven zijn
  stuggere crèmes en balsems zijn stuggere zalven. Balsems bestaan voor
  ¼ uit olie en ¾ uit andere harde stoffen zoals bijenwas. Des te meer
  olie je toevoegt des te zachter/vloeibaarder het resultaat is op
  kamertemperatuur.


3 Bereiding
═══════════

  Zorg er bij de bereiding voor dat je de etherische olieen zo laat
  mogelijk toe voegt bij de bereiding, anders vervluchtigen de aroma's,
  hierdoor krijg je een sterker middel. (Vooral bij bereiding onder
  hitte.)


3.1 Hitte
─────────

  Neem aan als vuistregel dat je de natuurlijke stoffen zo min mogelijk
  wilt verhitten. Je betaalt immers extra voor de koude persing. De
  stoffen blijven dan ook veel potenter en dit resulteert uiteindelijk
  in een potenter middel.


3.2 Mixen
─────────

  Sommige middelen kun je niet anders dan mixen met een mixer, omdat ze
  te stug zijn. /Kijk uit/ met mixen want mixen veroorzaakt wrijving →
  wrijving veroorzaakt warmte → warmte laat werkzame stoffen vervliegen.


4 De verschillen tussen cacaoboter en kokosolie
═══════════════════════════════════════════════

  Kokosolie heeft een net iets lagere smelttemperatuur [26℃, 26℃] dan
  Cacaoboter [34℃, 38℃]. Cacaoboter is veel overheersender qua geur —
  Doet denken aan chocola. —, als je dus cacaoboter gebruikt heb je meer
  etherische olie nodig als je hetzelfde geur effect wil krijgen dan bij
  kokos.


5 Receptuur van ons monster
═══════════════════════════

  Nota Bene: bij de receptuur moet je een afweging maken tussen hoe de
  textuur van het middel smeert en hoe de textuur van het middel
  aanziet.
  ━━━━━━━━━━━━━━┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
   Ingredienten │ Kokosolie  Sheaboter  Duindoorn  Jojoba  Bijenwas
  ──────────────┼───────────────────────────────────────────────────
   Hoeveelheid  │ 25g        12.5g      12.5g        12.5  1g
  ━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


6 Ingredienten
══════════════

  Dit zijn enkele ingrediënten die genoemd werden en hun effecten.
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
   Ingredient    Effect
  ──────────────────────────────────────────────────────
   Calendula     Goed voor de huid.
   Cisteroos     Antibacterieel.
   Teunis Bloem  Goed voor de huid.
   Neroli        Werkt goed tegen jeuk.
   Sandelhout    Werkt nog beter tegen jeuk dan Neroli.
   Neroli        Werkt goed bij een dunne huid.
   Wierook       Is ontstekingsremmend.
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


7 Veganisme
═══════════

  Bijenwas is niet veganistisch als antwoord hierop kan je dat weglaten,
  of eventueel meer sheaboter als vervanger gebruiken. Er moeten dan wel
  bij worden vermeld dat de textuur misschien minder mooi oogt en
  smeert.


8 Policies
══════════

  • Neem als houdbaarheidsdatum `min([houdbaarheids-data])'.
  • Informeer klanten dat ze geen vingers moeten gebruiken, maar dingen
    die ze weg kunnen werpen, of dingen die ze kunnen steriliseren
    (bijvoorbeeld met alcohol).
  • Ga batches nummeren, gebruik hiervoor *datver*.
  • Ga labels maken die dit in acht nemen:
    ⁃ houdbaarheidsdatum.
    ⁃ ingrediënten.
    ⁃ Veganistisch, vegetarisch, eventuele allergieën.
    ⁃ Naam product.
    ⁃ Batch №.
